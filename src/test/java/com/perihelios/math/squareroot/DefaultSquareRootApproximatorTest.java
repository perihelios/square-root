/*
 Copyright 2014, Perihelios LLC

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
package com.perihelios.math.squareroot;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import java.math.BigInteger;

import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;
import static java.math.BigInteger.valueOf;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

@RunWith(Enclosed.class)
public class DefaultSquareRootApproximatorTest {
	private static final BigInteger TWO = valueOf(2L);
	private static final BigInteger THREE = valueOf(3L);
	private static final BigInteger FOUR = valueOf(4L);
	private static final BigInteger FIVE = valueOf(5L);
	private static final BigInteger TWO_TO_THE_POWER_OF_14 = ONE.shiftLeft(14);
	private static final BigInteger TWO_TO_THE_POWER_OF_15 = valueOf(1L).shiftLeft(15);
	private static final BigInteger TWO_TO_THE_POWER_OF_30 = valueOf(1L).shiftLeft(30);
	private static final BigInteger TWO_TO_THE_POWER_OF_48 = valueOf(1L).shiftLeft(48);
	private static final BigInteger TWO_TO_THE_POWER_OF_49 = valueOf(1L).shiftLeft(49);
	private static final BigInteger TWO_TO_THE_POWER_OF_64 = ONE.shiftLeft(64);
	private static final BigInteger TWO_TO_THE_POWER_OF_128 = ONE.shiftLeft(128);

	public static class GeneralTests {
		@Test
		public void constructor_throwsExceptionWhenBitsOfPrecisionTooLarge() throws Exception {
			try {
				new DefaultSquareRootApproximator(33);
				fail("Expected exception " + IllegalArgumentException.class.getName());
			} catch (IllegalArgumentException expected) {
				assertThat(expected.getMessage(), is("Maximum bits of precision supported is 32, but 33 requested"));
			}
		}

		@Test
		public void constructor_throwsExceptionWhenBitsOfPrecisionTooSmall() throws Exception {
			try {
				new DefaultSquareRootApproximator(0);
				fail("Expected exception " + IllegalArgumentException.class.getName());
			} catch (IllegalArgumentException expected) {
				assertThat(expected.getMessage(), is("Minimum bits of precision supported is 1, but 0 requested"));
			}
		}

		@Test
		public void lowerBoundOfBigInteger_returnsLowerBound() throws Exception {
			DefaultSquareRootApproximator approximator = new DefaultSquareRootApproximator(10);

			assertThat(approximator.lowerBoundOf(FIVE), is(TWO));
		}

		@Test
		public void upperBoundOfBigInteger_returnsUpperBound() throws Exception {
			DefaultSquareRootApproximator approximator = new DefaultSquareRootApproximator(10);

			assertThat(approximator.upperBoundOf(FIVE), is(THREE));
		}

		@Test
		public void lowerBoundOfLong_returnsLowerBound() throws Exception {
			DefaultSquareRootApproximator approximator = new DefaultSquareRootApproximator(10);

			assertThat(approximator.lowerBoundOf(5L), is(2L));
		}

		@Test
		public void upperBoundOfLong_returnsUpperBound() throws Exception {
			DefaultSquareRootApproximator approximator = new DefaultSquareRootApproximator(10);

			assertThat(approximator.upperBoundOf(5L), is(3L));
		}
	}

	public static class LowPrecisionTest {
		private DefaultSquareRootApproximator approximator = new DefaultSquareRootApproximator(1);

		@Test
		public void squareRootBoundsBigInteger_behavesCorrectly() throws Exception {
			assertThat(approximator.boundsOf(ZERO), is(arrayContaining(ZERO, ZERO)));

			assertThat(approximator.boundsOf(ONE), is(arrayContaining(ONE, ONE)));

			assertThat(approximator.boundsOf(valueOf(3)), is(arrayContaining(ONE, valueOf(2))));
			assertThat(approximator.boundsOf(FOUR), is(arrayContaining(TWO, TWO)));
			assertThat(approximator.boundsOf(valueOf(5)), is(arrayContaining(TWO, FOUR)));

			assertThat(
				approximator.boundsOf(TWO_TO_THE_POWER_OF_30.subtract(ONE)),
				is(arrayContaining(
					TWO_TO_THE_POWER_OF_14,
					TWO_TO_THE_POWER_OF_15
				))
			);
			assertThat(approximator.boundsOf(TWO_TO_THE_POWER_OF_30), is(arrayContaining(TWO_TO_THE_POWER_OF_15, TWO_TO_THE_POWER_OF_15)));
			assertThat(
				approximator.boundsOf(TWO_TO_THE_POWER_OF_30.add(ONE)),
				is(arrayContaining(
					TWO_TO_THE_POWER_OF_15,
					TWO_TO_THE_POWER_OF_15.add(TWO_TO_THE_POWER_OF_15)
				))
			);
		}

		@Test
		public void squareRootBoundsLong_behavesCorrectly() throws Exception {
			assertThat(wrap(approximator.boundsOf(0L)), is(arrayContaining(0L, 0L)));

			assertThat(wrap(approximator.boundsOf(1L)), is(arrayContaining(1L, 1L)));

			assertThat(wrap(approximator.boundsOf(3L)), is(arrayContaining(1L, 2L)));
			assertThat(wrap(approximator.boundsOf(4L)), is(arrayContaining(2L, 2L)));
			assertThat(wrap(approximator.boundsOf(5L)), is(arrayContaining(2L, 4L)));

			assertThat(
				wrap(approximator.boundsOf((1L << 30) - 1L)),
				is(arrayContaining(
					1L << 14,
					1L << 15
				))
			);
			assertThat(wrap(approximator.boundsOf(1L << 30)), is(arrayContaining(1L << 15, 1L << 15)));
			assertThat(
				wrap(approximator.boundsOf((1L << 30) + 1L)),
				is(arrayContaining(
					1L << 15,
					1L << 16
				))
			);
		}
	}

	public static class MediumPrecisionTest {
		private DefaultSquareRootApproximator approximator = new DefaultSquareRootApproximator(16);

		@Test
		public void squareRootBoundsBigInteger_behavesCorrectly() throws Exception {
			assertThat(approximator.boundsOf(ZERO), is(arrayContaining(ZERO, ZERO)));

			assertThat(approximator.boundsOf(ONE), is(arrayContaining(ONE, ONE)));

			assertThat(approximator.boundsOf(THREE), is(arrayContaining(ONE, TWO)));
			assertThat(approximator.boundsOf(FOUR), is(arrayContaining(TWO, TWO)));
			assertThat(approximator.boundsOf(FIVE), is(arrayContaining(TWO, THREE)));

			assertThat(
				approximator.boundsOf(TWO_TO_THE_POWER_OF_30.subtract(ONE)),
				is(arrayContaining(
					TWO_TO_THE_POWER_OF_15.subtract(ONE),
					TWO_TO_THE_POWER_OF_15
				))
			);
			assertThat(approximator.boundsOf(TWO_TO_THE_POWER_OF_30), is(arrayContaining(TWO_TO_THE_POWER_OF_15, TWO_TO_THE_POWER_OF_15)));
			assertThat(
				approximator.boundsOf(TWO_TO_THE_POWER_OF_30.add(ONE)),
				is(arrayContaining(
					TWO_TO_THE_POWER_OF_15,
					TWO_TO_THE_POWER_OF_15.add(ONE)
				))
			);


			assertThat(
				approximator.boundsOf(TWO_TO_THE_POWER_OF_128.subtract(ONE)),
				is(arrayContaining(
					TWO_TO_THE_POWER_OF_64.subtract(TWO_TO_THE_POWER_OF_48),
					TWO_TO_THE_POWER_OF_64
				))
			);
			assertThat(approximator.boundsOf(TWO_TO_THE_POWER_OF_128), is(arrayContaining(TWO_TO_THE_POWER_OF_64, TWO_TO_THE_POWER_OF_64)));
			assertThat(
				approximator.boundsOf(TWO_TO_THE_POWER_OF_128.add(ONE)),
				is(arrayContaining(
					TWO_TO_THE_POWER_OF_64,
					TWO_TO_THE_POWER_OF_64.add(TWO_TO_THE_POWER_OF_49)
				))
			);
		}

		@Test
		public void squareRootBoundsLong_behavesCorrectly() throws Exception {
			assertThat(wrap(approximator.boundsOf(0L)), is(arrayContaining(0L, 0L)));

			assertThat(wrap(approximator.boundsOf(1L)), is(arrayContaining(1L, 1L)));

			assertThat(wrap(approximator.boundsOf(3L)), is(arrayContaining(1L, 2L)));
			assertThat(wrap(approximator.boundsOf(4L)), is(arrayContaining(2L, 2L)));
			assertThat(wrap(approximator.boundsOf(5L)), is(arrayContaining(2L, 3L)));

			assertThat(
				wrap(approximator.boundsOf((1L << 30) - 1L)),
				is(arrayContaining(
					(1L << 15) - 1L,
					(1L << 15)
				))
			);
			assertThat(wrap(approximator.boundsOf(1L << 30)), is(arrayContaining(1L << 15, 1L << 15)));
			assertThat(
				wrap(approximator.boundsOf((1L << 30) + 1L)),
				is(arrayContaining(
					1L << 15,
					(1L << 15) + 1L
				))
			);

			assertThat(
				wrap(approximator.boundsOf((1L << 50) - 1L)),
				is(arrayContaining(
					(1L << 25) - (1L << 9),
					(1L << 25)
				))
			);
			assertThat(wrap(approximator.boundsOf(1L << 50)), is(arrayContaining(1L << 25, 1L << 25)));
			assertThat(
				wrap(approximator.boundsOf((1L << 50) + 1L)),
				is(arrayContaining(
					1L << 25,
					(1L << 25) + (1L << 10)
				))
			);
		}
	}

	private static Long[] wrap(long[] values) {
		int length = values.length;
		Long[] wrapped = new Long[length];

		for (int i = 0; i < length; i++) {
			wrapped[i] = values[i];
		}

		return wrapped;
	}
}
