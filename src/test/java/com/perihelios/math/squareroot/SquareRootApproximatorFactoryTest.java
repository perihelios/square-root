/*
 Copyright 2014, Perihelios LLC

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
package com.perihelios.math.squareroot;

import org.junit.Test;

import static com.perihelios.math.squareroot.SquareRootApproximatorFactory.getDefaultAlgorithm;
import static com.perihelios.math.squareroot.SquareRootApproximatorFactory.getDefaultAlgorithmAtPrecision;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class SquareRootApproximatorFactoryTest {
	@Test
	public void getDefaultAlgorithm_behavesProperly() throws Exception {
		SquareRootApproximator approximator = getDefaultAlgorithm();

		assertThat(approximator, instanceOf(DefaultSquareRootApproximator.class));
		assertThat(approximator.upperBoundOf((1L << 60) + 1L), is((1L << 30) + (1L << 7)));
	}

	@Test
	public void getDefaultAlgorithmAtPrecision_behavesProperly() throws Exception {
		SquareRootApproximator approximator = getDefaultAlgorithmAtPrecision(4);

		assertThat(approximator, instanceOf(DefaultSquareRootApproximator.class));
		assertThat(approximator.upperBoundOf((1L << 10) + 1L), is((1L << 5) + (1L << 2)));
	}
}
