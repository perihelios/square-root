/*
 Copyright 2014, Perihelios LLC

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
package com.perihelios.math.squareroot;

import java.math.BigInteger;

import static com.perihelios.math.squareroot.BitCountUtils.bitLength;
import static java.math.BigInteger.ONE;
import static java.math.BigInteger.ZERO;
import static java.math.BigInteger.valueOf;
import static java.util.Arrays.binarySearch;

class DefaultSquareRootApproximator implements SquareRootApproximator {
	private final long[] lookupTable;
	private final long offset;
	private final int minimumBitLength;
	private final int maximumBitLength;

	public DefaultSquareRootApproximator(int bitsOfPrecision) {
		if (bitsOfPrecision > 32) {
			throw new IllegalArgumentException("Maximum bits of precision supported is 32, but " + bitsOfPrecision + " requested");
		}

		if (bitsOfPrecision < 1) {
			throw new IllegalArgumentException("Minimum bits of precision supported is 1, but " + bitsOfPrecision + " requested");
		}

		long maximum = 1 << bitsOfPrecision;
		long minimum = maximum >>> 1;

		lookupTable = new long[(int) minimum];
		offset = minimum;
		maximumBitLength = bitsOfPrecision << 1;
		minimumBitLength = maximumBitLength - 2;

		for (long squareRoot = minimum; squareRoot < maximum; squareRoot++) {
			long square = squareRoot * squareRoot;

			lookupTable[(int) (squareRoot - minimum)] = square;
		}
	}

	@Override
	public BigInteger[] boundsOf(BigInteger value) {
		if (value.equals(ZERO)) {
			return new BigInteger[] { ZERO, ZERO };
		}

		int bitLength = value.bitLength();

		if (bitLength > maximumBitLength) {
			return scaleDownAndLookup(value, bitLength);
		} else if (bitLength <= minimumBitLength) {
			return scaleUpAndLookup(value, bitLength);
		} else {
			return lookupDirectly(value);
		}
	}

	@Override
	public BigInteger lowerBoundOf(BigInteger value) {
		return boundsOf(value)[0];
	}

	@Override
	public BigInteger upperBoundOf(BigInteger value) {
		return boundsOf(value)[1];
	}

	@Override
	public long[] boundsOf(long value) {
		if (value == 0L) {
			return new long[] { 0L, 0L };
		}

		int bitLength = bitLength(value);

		if (bitLength > maximumBitLength) {
			return scaleDownAndLookup(value, bitLength);
		} else if (bitLength <= minimumBitLength) {
			return scaleUpAndLookup(value, bitLength);
		} else {
			return lookupDirectly(value);
		}
	}

	@Override
	public long lowerBoundOf(long value) {
		return boundsOf(value)[0];
	}

	@Override
	public long upperBoundOf(long value) {
		return boundsOf(value)[1];
	}

	private BigInteger[] scaleDownAndLookup(BigInteger value, int bitLength) {
		int squareRootShiftAmount = (bitLength - maximumBitLength + 1) >>> 1;
		int shiftAmount = squareRootShiftAmount << 1;
		int rightZeros = rightZeros(value);
		long lookupKey = value.shiftRight(shiftAmount).longValue();

		int index = binarySearch(lookupTable, lookupKey);

		BigInteger lower;
		BigInteger upper;

		if (index < 0) {
			long adjusted = (-(index + 1) + offset - 1);

			lower = valueOf(adjusted).shiftLeft(squareRootShiftAmount);
			upper = lower.add(ONE.shiftLeft(squareRootShiftAmount));
		} else {
			long adjusted = (index + offset);

			if (shiftAmount <= rightZeros) {
				upper = lower = valueOf(adjusted).shiftLeft(squareRootShiftAmount);
			} else {
				lower = valueOf(adjusted).shiftLeft(squareRootShiftAmount);
				upper = lower.add(ONE.shiftLeft(squareRootShiftAmount));
			}
		}

		return new BigInteger[] { lower, upper };
	}

	private BigInteger[] scaleUpAndLookup(BigInteger value, int bitLength) {
		int squareRootShiftAmount = (minimumBitLength - bitLength + 2) >> 1;
		int shiftAmount = squareRootShiftAmount << 1;
		long lookupKey = value.longValue() << shiftAmount;

		int index = binarySearch(lookupTable, lookupKey);

		long adjusted;
		BigInteger lower;
		BigInteger upper;

		if (index < 0) {
			adjusted = (-(index + 1L) + offset - 1L) >>> squareRootShiftAmount;
			lower = valueOf(adjusted);
			upper = valueOf(adjusted + 1L);
		} else {
			adjusted = (index + offset) >>> squareRootShiftAmount;
			upper = lower = valueOf(adjusted);
		}

		return new BigInteger[] { lower, upper };
	}

	private BigInteger[] lookupDirectly(BigInteger value) {
		long lookupKey = value.longValue();
		int index = binarySearch(lookupTable, lookupKey);

		long adjusted;
		BigInteger lower;
		BigInteger upper;

		if (index < 0) {
			adjusted = -(index + 1) + offset - 1L;
			lower = valueOf(adjusted);
			upper = valueOf(adjusted + 1L);
		} else {
			adjusted = index + offset;
			upper = lower = valueOf(adjusted);
		}

		return new BigInteger[] { lower, upper };
	}

	private long[] scaleDownAndLookup(long value, int bitLength) {
		int squareRootShiftAmount = (bitLength - maximumBitLength + 1) >>> 1;
		int shiftAmount = squareRootShiftAmount << 1;
		int rightZeros = BitCountUtils.rightZeros(value);
		long lookupKey = value >>> shiftAmount;

		int index = binarySearch(lookupTable, lookupKey);

		long lower;
		long upper;

		if (index < 0) {
			long adjusted = (-(index + 1) + offset - 1);

			lower = adjusted << squareRootShiftAmount;
			upper = lower + (1 << squareRootShiftAmount);
		} else {
			long adjusted = (index + offset);

			if (shiftAmount <= rightZeros) {
				upper = lower = adjusted << squareRootShiftAmount;
			} else {
				lower = adjusted << squareRootShiftAmount;
				upper = lower + (1 << squareRootShiftAmount);
			}
		}

		return new long[] { lower, upper };
	}

	private long[] scaleUpAndLookup(long value, int bitLength) {
		int squareRootShiftAmount = (minimumBitLength - bitLength + 2) >> 1;
		int shiftAmount = squareRootShiftAmount << 1;
		long lookupKey = value << shiftAmount;

		int index = binarySearch(lookupTable, lookupKey);

		long adjusted;
		long lower;
		long upper;

		if (index < 0) {
			adjusted = (-(index + 1L) + offset - 1L) >>> squareRootShiftAmount;
			lower = adjusted;
			upper = adjusted + 1L;
		} else {
			adjusted = (index + offset) >>> squareRootShiftAmount;
			upper = lower = adjusted;
		}

		return new long[] { lower, upper };
	}

	private long[] lookupDirectly(long value) {
		int index = binarySearch(lookupTable, value);

		long adjusted;
		long lower;
		long upper;

		if (index < 0) {
			adjusted = -(index + 1L) + offset - 1L;
			lower = adjusted;
			upper = adjusted + 1L;
		} else {
			adjusted = index + offset;
			upper = lower = adjusted;
		}

		return new long[] { lower, upper };
	}

	private static int rightZeros(BigInteger value) {
		int count = 0;
		int bitLength = value.bitLength();

		while (count < bitLength && !value.testBit(count)) {
			count++;
		}

		return count;
	}
}
