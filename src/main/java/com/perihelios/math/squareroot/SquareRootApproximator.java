/*
 Copyright 2014, Perihelios LLC

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
package com.perihelios.math.squareroot;

import java.math.BigInteger;

/**
 * Utility that approximates the integer portion of the square root of an integer by providing lower and upper bounds that encompass the square root.
 * <p/>
 * Both {@link java.math.BigInteger} and {@code long} are supported as input types. All bounds given are inclusive; that is, if {@code root} is the
 * square root of of {@code value}, {@code lower <= root <= upper}. For methods returning arrays, the first element is {@code lower} and the second
 * {@code upper}.
 * <p/>
 * The precision of the approximation, or the "tightness" of the bounds for a particular value, is implementation dependent. In an optimal case,
 * {@code lower == upper} when {@code value} is a perfect square, and {@code lower = upper - 1} when {@code value} is an imperfect square. There is
 * generally a trade-off between resource usage (whether processing or memory) and precision; certain algorithms may favor one or the other, or
 * may be configurable.
 * <p/>
 * <strong>Note:</strong> For most implementations, it is expected that {@code boundsOf()} will execute more efficiently than calling
 * {@code lowerBoundOf()} and {@code upperBoundOf()} separately.
 */
public interface SquareRootApproximator {
	/**
	 * Calculates lower and upper bounds of square root.
	 * <p/>
	 *
	 * @param value Value for which to calculate bounds
	 * @return Two-element array of lower and upper bounds, where lower bound is at index {@code 0} and upper bound at index {@code 1}
	 */
	BigInteger[] boundsOf(BigInteger value);

	/**
	 * Calculates lower bound of square root.
	 * <p/>
	 * <strong>Note:</strong> For most implementations, it is expected that {@code boundsOf()} will execute more efficiently than calling
	 * {@code lowerBoundOf()} and {@code upperBoundOf()} separately.
	 *
	 * @param value Value for which to calculate lower bound
	 * @return Lower bound of square root of {@code value}
	 */
	BigInteger lowerBoundOf(BigInteger value);

	/**
	 * Calculates upper bound of square root.
	 * <p/>
	 * <strong>Note:</strong> For most implementations, it is expected that {@code boundsOf()} will execute more efficiently than calling
	 * {@code lowerBoundOf()} and {@code upperBoundOf()} separately.
	 *
	 * @param value Value for which to calculate upper bound
	 * @return Upper bound of square root of {@code value}
	 */
	BigInteger upperBoundOf(BigInteger value);

	/**
	 * Calculates lower and upper bounds of square root.
	 *
	 * @param value Value for which to calculate bounds
	 * @return Two-element array of lower and upper bounds, where lower bound is at index {@code 0} and upper bound at index {@code 1}
	 */
	long[] boundsOf(long value);

	/**
	 * Calculates lower bound of square root.
	 * <p/>
	 * <strong>Note:</strong> For most implementations, it is expected that {@code boundsOf()} will execute more efficiently than calling
	 * {@code lowerBoundOf()} and {@code upperBoundOf()} separately.
	 *
	 * @param value Value for which to calculate lower bound
	 * @return Lower bound of square root of {@code value}
	 */
	long lowerBoundOf(long value);

	/**
	 * Calculates upper bound of square root.
	 * <p/>
	 * <strong>Note:</strong> For most implementations, it is expected that {@code boundsOf()} will execute more efficiently than calling
	 * {@code lowerBoundOf()} and {@code upperBoundOf()} separately.
	 *
	 * @param value Value for which to calculate upper bound
	 * @return Upper bound of square root of {@code value}
	 */
	long upperBoundOf(long value);
}
