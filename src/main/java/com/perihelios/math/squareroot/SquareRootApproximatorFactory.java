/*
 Copyright 2014, Perihelios LLC

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/
package com.perihelios.math.squareroot;

public class SquareRootApproximatorFactory {
	private static final int DEFAULT_PRECISION = 24;

	public static SquareRootApproximator getDefaultAlgorithm() {
		return new DefaultSquareRootApproximator(DEFAULT_PRECISION);
	}

	public static SquareRootApproximator getDefaultAlgorithmAtPrecision(int precision) {
		return new DefaultSquareRootApproximator(precision);
	}

	// Prevent instantiation of this factory class
	private SquareRootApproximatorFactory() {}
}
